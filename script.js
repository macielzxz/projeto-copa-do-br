class Time{
    constructor (localizacao, time1, time2){
      this.localizacao = localizacao;
      this.time1 = time1;
      this.time2 = time2;
    }
    imprimirTime(){
      console.log('${this.localizacao}, ${this.time1}, ${this.time2}')
    }
  }
  let time = new Time("Rio De Janeiro","Flamengo", "Vasco")
  
  class Estadio{
    constructor(nome, localizacao,capacidade){
      this.nome = nome;
      this.localizacao = localizacao;
      this.capacidade = capacidade;
    }
      imprimirEstadio() {
        console.log('${this.nome}, ${this.localizacao}, ${this.capacidade}')
      }
    }
  let estadio = new Estadio("Maracana", "Rio De Janeiro", "70.000 mil");
  
  class Jogador{
    constructor(nomevasco, numerovasco, nomeflamengo, numeroflamengo){
      this.nomevasco = nomevasco;
      this.numerovasco = numerovasco;
      this.nomeflamengo = nomeflamengo;
      this.numeroflamengo = numeroflamengo;
      
    }
      imprimirJogador() {
        console.log('${this.nomevasco}, ${this.numerovasco}, ${this.nomeflamengo}, ${this.numeroflamengo}')
      }
    }
  let jogador = new Jogador("Ribamar", "9", "Gabriel Barborsa", "9");
  
  class Jogo{
    constructor(mandante, visitante, horario, local, arbitritropricipal, duracao, arbitroauxiliar, bandeirinha1, banderinha2, arbitrodevideo){
      this.mandante = mandante;
      this.visitante = visitante ;
      this.horario = horario;
      this.local = local;
      this.arbitro = arbitro;
      this.duracao = duracao;
      this.arbitroauxiliar = arbitroauxiliar;
      this.bandeirinha1 = bandeirinha1;
      this.bandeirinha2 = bandeirinha2;
      this.arbitrodevideo = arbitrodevideo;
      
    }
      imprimirjogo() {
        console.log('${this.mandante}, ${this.visitante}, ${this.horario} ${this.local}, ${this.arbitro}, ${this.duracao}, ${this.arbitroauxiliar}, ${this.bandeirinha1}, ${this.bandeirinha2}, ${this.arbitrodevideo}')
      }
    }
  let jogo = new Jogo("Flamengo", "Vasco", "16 horas", "Maracana", "Anderson Daronco", "90 minutos", "Acacio Menezes Leao","Raphael Claus", "Rodrigo Figueiredo", "Ramon Abatti")  
  